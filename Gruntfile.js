module.exports = function (grunt) {
  grunt.initConfig({
    watch: {
      options: {
        livereload: true
      },
      html: {
        files: ['*.html', '**/*.html']
      }
    },

    connect: {
      site: {
        options: {
          port: 5566,
          base: './'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask('default', ['connect:site', 'watch']);
};
