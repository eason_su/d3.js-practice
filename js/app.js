window.d3Ex = {};

d3Ex.lineChart = function () {
  var margin = {top: 20, right: 20, bottom: 30, left: 50},
      width = 960 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom;

  var parseDate = d3.time.format("%d-%b-%y").parse;

  var x = d3.time.scale()
      .range([0, width]);

  var y = d3.scale.linear()
      .range([height, 0]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left");

  var line = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.close); });

  var svg = d3.select(".svn-wrap").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  d3.tsv("res/data.tsv", function(error, data) {
    data.forEach(function(d) {
      d.date = parseDate(d.date);
      d.close = +d.close;
    });

    x.domain(d3.extent(data, function(d) { return d.date; }));
    y.domain(d3.extent(data, function(d) { return d.close; }));

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
      .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Price ($)");

    svg.append("path")
        .datum(data)
        .attr("class", "line")
        .attr("d", line);
  });
};


d3Ex.pieChart = function () {
  var width = 960,
      height = 600,
      radius = Math.min(width, height) / 2;

  var color;

  var arc = d3.svg.arc()
      .outerRadius(radius - 10)
      .innerRadius(0);

  var pie = d3.layout.pie()
      .sort(null)
      .value(function(d) { return d.count; });

  var svg = d3.select(".pie-chart-wrap").append("svg")
      .attr("width", width)
      .attr("height", height)
    .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

  var genColorUnit = function () {
    var unit = (~~(Math.random() * 256)).toString(16);
    if (unit.length === 1) {
      return '0' + unit;
    }
    return unit;
  };

  var genColor = function () {
    return '#' + genColorUnit() + genColorUnit() + genColorUnit();
  };

  d3.json("res/result.json", function(error, data) {
    var eventTypeMap = {},
    eventTypes = [],
    colors = [];

    data.forEach(function(d) {
      eventTypeMap[d.event_type] = (eventTypeMap[d.event_type] || 0) + 1
    });

    for (var type in eventTypeMap) {
      eventTypes.push({label: type, count: eventTypeMap[type]});
      colors.push(genColor())
    }

    color = d3.scale.ordinal().range(colors);

    console.log(JSON.stringify(eventTypes, null, 2));
    console.log(eventTypes.length);

    var g = svg.selectAll(".arc")
        .data(pie(eventTypes))
      .enter().append("g")
        .attr("class", "arc");

    g.append("path")
        .attr("d", arc)
        .style("fill", function(d) { return color(d.data.label); });

    g.append("text")
        .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
        .attr("dy", ".35em")
        .style("text-anchor", "middle")
        .text(function(d) { return d.data.label + ': ' + d.data.count; });

  });
};


(function () {
  var d3App, router, controller;

  var controllerProvider = function ($stateParams, $controller, $rootScope) {
    $rootScope.isMainPage = false;

    if ($stateParams.ctlr) {
      return $stateParams.name;
    }
  };

  angular.module('d3App', ['router', 'controller', 'ngAnimate']);


  angular.module('router', ['ui.router'])
    .config(function ($urlRouterProvider, $stateProvider) {
      $urlRouterProvider.otherwise('/main');

      $stateProvider
        .state('main', {
          url: '/main',
          template: '',
          controller: 'main'
        })
        .state('ex', {
          url: '/example/:name/:ctlr',
          templateUrl: function (params) {
            return 'example/' + params.name + '.html';
          },
          controllerProvider: controllerProvider
        })
        .state('ex_empty_wrap', {
          url: '/example/:name/:singleWrap/:ctlr/',
          template: function (params) {
            return '<div class="' + params.singleWrap + '">';
          },
          controllerProvider: controllerProvider
        });
    });


  angular.module('controller', [])
    .controller('main', function ($rootScope) {
      $rootScope.isMainPage = true;
    })

    .controller('d3-line-chart', d3Ex.lineChart)

    .controller('svg-viewbox', function ($scope) {
      $scope.attrList = [
        {
          label: '預設',
          viewBox: 'none',
          preserveAspectRatio: 'none'
        },
        {
          label: '完全填滿 - 比例不同',
          viewBox: '230 175 200 200',
          preserveAspectRatio: 'none'
        },
        {
          label: '完全填滿 - 比例相同',
          viewBox: '230 175 200 150',
          preserveAspectRatio: 'none'
        },
        {
          label: '瘦高',
          viewBox: '230 100 200 400',
          preserveAspectRatio: 'xMinYMin meet'
        },
        {
          label: '瘦高',
          viewBox: '230 100 200 400',
          preserveAspectRatio: 'xMidYMin meet'
        },
        {
          label: '瘦高',
          viewBox: '230 100 200 400',
          preserveAspectRatio: 'xMidYMid meet'
        },
        {
          label: '瘦高',
          viewBox: '230 100 200 400',
          preserveAspectRatio: 'xMaxYMin meet'
        },
        {
          label: '瘦高',
          viewBox: '230 100 200 400',
          preserveAspectRatio: 'xMinYMin slice'
        },
        {
          label: '瘦高',
          viewBox: '230 100 200 400',
          preserveAspectRatio: 'xMinYMid slice'
        },
        {
          label: '瘦高',
          viewBox: '230 100 200 400',
          preserveAspectRatio: 'xMinYMax slice'
        },
        {
          label: '隨容器調整大小',
          width: '100%',
          height: '100%',
          viewBox: '0 0 640 480',
          preserveAspectRatio: 'none'
        }
      ];


      $scope.change = function (idx) {
        var attr = $scope.attrList[idx];
        attr.width = attr.width || 640;
        attr.height = attr.height || 480;
        $scope.svg = attr;
      };

      $scope.change(0);;
    })

    .controller('d3-pie-chart', d3Ex.pieChart);
}());
